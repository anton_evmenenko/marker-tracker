#ifndef _SERIAL_
#define _SERIAL_

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <iostream>

int openSerialPort(char* portName, int speed, int parity, int shouldBlock);
void closeSerialPort(int fd);
size_t writeToSerialPort(int fd, char* buffer, size_t bufferLength);

#endif
