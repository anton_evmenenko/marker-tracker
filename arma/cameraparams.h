#ifndef _CAMERAPARAMS_H
#define _CAMERAPARAMS_H
#include <opencv2/core/core.hpp>
using namespace cv;

    CvMat* intrinsic = (CvMat*)cvLoad("/home/anton/projects/marker_tracker/intrinsic.xml");
    CvMat* distor = (CvMat*)cvLoad("/home/anton/projects/marker_tracker/distortion.xml");

	Mat cameraMatrix = cvarrToMat(intrinsic);
	Mat distortions = cvarrToMat(distor);



#endif


