#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "arma/cameraparams.h"
#include "arma/patterndetector.h"
#include <thread>
#include <numeric>
#include <glib.h>
#include <libv4l2.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <signal.h>
#include "serial/serial.h"

#define DEV_NAME "/dev/ttyUSB0"
#define PAT_SIZE 64 //equal to pattern_size variable (see below)
#define MAX_QUEUE_SIZE 10
#define FRAME_WIDTH 320
#define FRAME_HEIGHT 240
#define WORKER_THREADS 1
#define SERIAL_OUTPUT 1

char* filename="/home/anton/projects/marker_tracker/aruco.png";

volatile bool runCamera = true;
volatile bool runProcessing = true;
GAsyncQueue* framesFromCamera;
cv::VideoCapture capture;
std::vector<cv::Mat> patternLibrary;
unsigned long sendedFrameId = 0;
unsigned long receivedFrameId = 0;
unsigned getNextFrameThreadSleepMs = 0;
unsigned previousQueueLength = 0;
int fd;

int loadPattern(const char* filename, std::vector<cv::Mat>& library, int& patternCount){
    Mat img = imread(filename,0);

    if(img.cols!=img.rows){
        std::cerr << "Not a square pattern" << std::endl;
        return -1;
    }

    int msize = PAT_SIZE;

    Mat src(msize, msize, CV_8UC1);
    Point2f center((msize-1)/2.0f,(msize-1)/2.0f);
    Mat rot_mat(2,3,CV_32F);

    resize(img, src, Size(msize,msize));
    Mat subImg = src(Range(msize/4,3*msize/4), Range(msize/4,3*msize/4));
    library.push_back(subImg);

    rot_mat = getRotationMatrix2D( center, 90, 1.0);

    for (int i=1; i<4; i++){
        Mat dst= Mat(msize, msize, CV_8UC1);
        rot_mat = getRotationMatrix2D( center, -i*90, 1.0);
        warpAffine( src, dst , rot_mat, Size(msize,msize));
        Mat subImg = dst(Range(msize/4,3*msize/4), Range(msize/4,3*msize/4));
        library.push_back(subImg);
    }

    patternCount++;
    return 1;
}

void getNextFrame()
{
    g_async_queue_ref(framesFromCamera);

    cv::Mat frame;

    while(runCamera) {
        //auto start = std::chrono::high_resolution_clock::now( );

        cv::Mat *buffer = new cv::Mat();
        capture >> frame;
        cv::resize(frame, *buffer, cv::Size(FRAME_WIDTH, FRAME_HEIGHT), 0, 0, cv::INTER_LINEAR);
        g_async_queue_push(framesFromCamera, (gpointer)buffer);

        //unsigned ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();

        //float fps = 1000. / (float)ms;
        //std::cout << "FPS = " << fps << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(getNextFrameThreadSleepMs));
    }

    g_async_queue_unref(framesFromCamera);
    return;
}

void byteToHexString(char* buffer, char x)
{
    char b1 = (x >> 4) & 0x0F;
    char b2 = x & 0x0F;
    buffer[0] = (b1 < 10) ? b1 + '0' : b1 - 10 + 'A';
    buffer[1] = (b2 < 10) ? b2 + '0' : b2 - 10 + 'A';
}

void int16ToHexString(char* buffer, int16_t x)
{
    byteToHexString(buffer, x >> 8);
    byteToHexString(buffer + 2, x);
}


void processFrame()
{
    g_async_queue_ref(framesFromCamera);

    int norm_pattern_size = PAT_SIZE;
    double fixed_thresh = 40;
    double adapt_thresh = 5;//non-used with FIXED_THRESHOLD mode
    int adapt_block_size = 45;//non-used with FIXED_THRESHOLD mode
    double confidenceThreshold = 0.35;
    int mode = 2;//1:FIXED_THRESHOLD, 2: ADAPTIVE_THRESHOLD


    ARma::PatternDetector myDetector( fixed_thresh, adapt_thresh, adapt_block_size, confidenceThreshold, norm_pattern_size, mode);
    std::vector<ARma::Pattern> detectedPattern;

    while(runProcessing){
        unsigned long frameId;
        {
            // TODO : add mutex
            frameId = sendedFrameId;
            sendedFrameId++;
        }

        unsigned queueLength = g_async_queue_length(framesFromCamera);

        if (queueLength > previousQueueLength) {
            getNextFrameThreadSleepMs += 1;
            std::cout << "Camera thread sleep time increased. Now = " << getNextFrameThreadSleepMs << " ms." << std::endl;
        }

        previousQueueLength = queueLength;

        if (queueLength > MAX_QUEUE_SIZE) {
            std::cerr << "Queue is too long" << std::endl;
            runProcessing = false;
        }

        Mat* imgMat = ((cv::Mat*)(g_async_queue_pop(framesFromCamera)));

        myDetector.detect(*imgMat, cameraMatrix, distortions, patternLibrary, detectedPattern);

        if (detectedPattern.size() > 0) {
            Point2f center = detectedPattern.at(0).getCenter(cameraMatrix, distortions);
            //circle(*imgMat, center, 5, cvScalar(255,255,0), 3 );
            //detectedPattern.at(0).drawCube(*imgMat, cameraMatrix, distortions);
            float deltaXf = ( center.x - FRAME_WIDTH / 2. ) / ( FRAME_WIDTH / 2. );
            float deltaYf = -( center.y - FRAME_HEIGHT / 2. ) / ( FRAME_HEIGHT / 2. );
            int max = 255;
            int16_t deltaXi = (int16_t)(deltaXf * max);
            int16_t deltaYi = (int16_t)(deltaYf * max);
            std::cout << "y " << std::to_string(deltaXi) << " " << std::to_string(deltaYi) << std::endl;
#ifdef SERIAL_OUTPUT
            if ( fd >= 0 ) {
                char serialBuffer[9];
                int16ToHexString(serialBuffer, deltaXi);
                int16ToHexString(serialBuffer + 4, deltaYi);
                serialBuffer[8] = '\n';
                writeToSerialPort(fd, serialBuffer, 9);
            }
#endif
        } else {
            std::cout << "n" << std::endl;
#ifdef SERIAL_OUTPUT
            if ( fd >= 0 ) {
                char serialBuffer[] = {'n', '\n'};
                writeToSerialPort(fd, serialBuffer, 2);
            }
#endif
        }

        //imshow("result", *imgMat);

        detectedPattern.clear();

        //char c = cvWaitKey(1);
        //if (c == 27) {
        //    runProcessing = false;
        //}

        delete imgMat;
    }

    g_async_queue_unref(framesFromCamera);
    return;
}


void SIGINTHandler(int dummy) {
    runProcessing = false;
}

int main(int argc, char** argv){
    signal(SIGINT, SIGINTHandler);

#ifdef SERIAL_OUTPUT
    fd = openSerialPort(DEV_NAME, B9600, 0, 0);
#endif

    int patternCount=0;
    loadPattern(filename, patternLibrary, patternCount);

    //magic from http://stackoverflow.com/questions/15035420/configuring-camera-properties-in-new-ocv-2-4-3
    int descriptor = v4l2_open("/dev/video0", O_RDWR);
    v4l2_control control;
    control.id = V4L2_CID_EXPOSURE_AUTO;
    control.value = V4L2_EXPOSURE_MANUAL;
    if(v4l2_ioctl(descriptor, VIDIOC_S_CTRL, &control))
        std::cerr << "Failed to set manual exposure mode." << std::endl;

    capture = cv::VideoCapture(0);

    framesFromCamera = g_async_queue_new();

    std::thread cameraThread(getNextFrame);

    sleep(1);

    std::vector<std::thread> threads;

    for (int i = 0; i < WORKER_THREADS; ++i)
        threads.push_back(std::thread(processFrame));

    for (auto it = threads.begin(); it != threads.end(); ++it)
        it->join();

    runCamera = false;

    cameraThread.join();
    capture.release();

#ifdef SERIAL_OUTPUT
    closeSerialPort(fd);
#endif

    std::cout << "Shut down correctly." << std::endl;

    return 0;
}
